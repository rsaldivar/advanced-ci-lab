# Dynamic Pipeline

This challenge will generate a pipeline dynamically that contains build and test jobs for all of the project directories we specify.  Instead of a hands-on exercise, we have already made the necessary changes for you.  Follow the instructions below to experience it.

## Step 01 - View the dynamic pipeline components
1. Use the menu to navigate to **Code -> Repository**.  

2. At the top of the repository page, you will notice a branch selector.  
- GitLab displays the source for the **_main_** branch by default.  
- Use the selector to switch to the **_4-dynamic-pipeline_** branch.

3. Notice there is a new file in the root of the repository **_template.yml_**.  
- Click the file to view its contents - it's almost the same CI template we used in the last exercise.  
- However, you will notice that the parallel:matrix section is a bit different.  

You will see in both the **_build_** and **_get_container_** jobs:

```plaintext
parallel:
    matrix:
      - PROJECT_DIR: $PROJECT_DIRS
```
and `$PROJECT_DIRS` is not defined within the **_template.yml_** file.

4. Click the **_.gitlab-ci.yml_** file to view its contents.

  - Notice the global variables block

  ```plaintext
  variables:
  PROJECT_DIRS: '["python1", "python2"]'
  ```
- This is where the array of project directories for which we want to run build and test jobs is defined.  

- But GitLab does not have the ability to expand variables within the `parallel:matrix` section.  

- However, we have a trick that allows us to use the variables by dynamically generating a child pipeline.

5. Review the jobs defined in the **_.gitlab_ci.yml_** file.  

  - In the **_process:template:_** job, the `envsubst` command that is part of the linux open source `gettext-base` package is used to replace the `$PROJECT_DIRS` in the **_template.yml_** file with the `$PROJECT_DIRS` value defined here.  
  
  - The result is written to a new file **_template.gitlab-ci.yml_** that is saved as an artifact
  - The **_trigger:template:_** job triggers the jobs defined by the **_template.gitlab-ci.yml_** file as a downstream pipeline

6. Use the menu to navigate to the **Build-> Pipelines** page.  

- Use the **Run Pipeline** button in the upper right-hand corner to manually run a pipeline.  

- Use the branch selector to choose the **_4-dynamic-pipeline_** branch.  

- Then click **Run Pipeline**.  Watch the pipeline run and view what jobs are created.

## BONUS
See all of the concepts we've learned today come together in a realistic example.  In reality, when we have a monorepo with multiple projects to build, we would want to automate the discovery of the projects to build and collection of the directory names in an array, and then use a dynamic pipeline to generate the appropriate jobs to run.  You can see that example implemented in the [Dockerfiles project](https://gitlab.com/sandlin/dockerfiles) created by a GitLab Solutions Architect.


